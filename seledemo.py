#coding:gbk;
import requests
import chardet
from bs4 import BeautifulSoup#加载所需的第三方的库
import pandas as pd
import json
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium import webdriver

import re
from random import random
import time
from selenium.common.exceptions import NoSuchElementException

#begin: change to current dir
from os import path,chdir,getcwd,system
from sys import argv
s1=path.dirname(argv[0]) 
cwd=s1 if s1!="" else getcwd()
chdir(cwd)
del s1
#end:  change to current dir
import atexit
import pickle
import os
CACHEFILEOUT='seledemo.pickle'
def savecacheout():  #防止
    print("Dumping cache to %s..."%CACHEFILEOUT)
    bigger=True
    with open(CACHEFILEOUT+".tmp", "wb") as o:
        pickle.dump([titles,abstracts],o)
    if path.exists(CACHEFILEOUT):
        bigger=path.getsize(CACHEFILEOUT)<path.getsize(CACHEFILEOUT+".tmp")
        if bigger:  #tmp contains more
            os.remove(CACHEFILEOUT)
            os.rename(CACHEFILEOUT+".tmp",CACHEFILEOUT)
        else:
            os.remove(CACHEFILEOUT+".tmp")
    else:
        os.rename(CACHEFILEOUT+".tmp",CACHEFILEOUT)
    if bigger:
        df=pd.DataFrame({'titles':titles,'abstracts':abstracts})
        df.to_csv('abstracts.csv') 
atexit.register(savecacheout)
web = webdriver.Chrome() # 声明一个谷歌浏览器的对象

url = 'https://xueshu.baidu.com/'

web.get(url) # 模拟用户在浏览器中向服务器发送请求
# 定位搜索框，输入“”

web.find_element_by_css_selector('#kw').send_keys('计算机软件反向工程')
web.find_element_by_css_selector('#su').click() # 点击搜索
time.sleep(0.9+0.5*random())  #
time.sleep(0.9+0.5*random()) 
#web.find_element_by_css_selector('#popop > div > div.panel_tags.mk > div > span > em').click()
#it is called 'selector' in chrome
time.sleep(0.9+0.5*random()) 
#web.find_element_by_css_selector('#popop > div > div.but_box > span').click()

abstracts = [] # 存储数据
j=0
titles=[]
for i in range(50):  #50 pages
    time.sleep(1.9+0.5*random()) 
    while 1: # for each item
        j=j+1
        xpath1='//*[@id="%d"]/div[1]/h3/a'%j
        
        try:
            print(j)
            tmp=web.find_element_by_xpath(xpath1)
            titles.append(tmp.get_attribute('innerHTML'))
            #print(titles[-1])
            FP=web.current_window_handle # 当前窗口
            tmp.click() # 1st no bug
            #need switch to the new tab
            # print(web.current_url)
            nn = web.window_handles # 查看当前浏览器的所有窗口
            nn=nn.copy()
            nn.remove(FP)
            web.switch_to_window(nn[0])
            selector2='#dtl_l > div.main-info > div.c_content > div.abstract_wr > p.abstract'
            try:
                tmp=web.find_element_by_css_selector(selector2)
            except NoSuchElementException:
            #no abstract
                iscaptcha=0
                if 'xueshu.baidu.com' not in web.current_url:
                    iscaptcha=1
                    #cert
                    input('if captcha is ok, press enter here')
                    try:
                        tmp=web.find_element_by_css_selector(selector2)
                    except NoSuchElementException:
                        iscaptcha=0
                if not iscaptcha:
                    web.close()
                    web.switch_to_window(FP)
                    abstracts.append("")
                    continue
                # mark the switch to the old tab
            abstracts.append(tmp.get_attribute('innerHTML'))
            #print(abstracts[-1])
            time.sleep(1.5+1.5*random())
            web.close()
            # mark the switch to the old tab
            web.switch_to_window(FP)
            #print(titles,abstracts)  
            time.sleep(1.4+1.5*random())
        except NoSuchElementException: break 
        #no more item

    #去下一页
    time.sleep(10+1*random())
    web.delete_all_cookies()
    print('next page')
    selector1='#page > a.n > i'
    m=11
    while 1:
        try:
            xpath2='//*[@id="page"]/a[%d]/i'%m
            #tmp=web.find_element_by_css_selector(selector1)
            tmp=web.find_element_by_xpath(xpath2)      
            break
        except NoSuchElementException: m-=1;
    tmp=web.find_element_by_xpath(xpath2)    
    #for k in tmp:
    #    if k.get_attribute('innerHTML').click()
    LASTP=web.current_url
    tmp.click()
    if LASTP==web.current:
        break
               
"""
if 0:    
    b = [i.text for i in web.find_elements_by_css_selector('body > div:nth-child(4) > div.j_result > div > div.leftbox > div:nth-child(4) > div.j_joblist > div:nth-child(1) > a > p.t > span.jname.at')] # 职位名称
    # 定义一个等待的对象
    wait = WebDriverWait(web,10) 
    # 调用等待对象，设置等待条件
    button = wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR,'body > div:nth-child(4) > div.j_result > div > div.j_tlc > div.tright > div.rt.rt_page')))
    button = wait.until(EC.element_to_be_clickable((By.XPATH,'/html/body/div[1]/div[4]/div[3]/p/a[8]')))
    #body > div:nth-child(4) > div.j_result > div > div.leftbox > div:nth-child(4) > div.j_joblist > div:nth-child(1) > a > p.t > span.jname.at
    # 点击下一页，实现翻页
    try:
        button.click()
    except:pass
    data = pd.DataFrame({'职位名称':b})
    abstracts = pd.concat([abstracts,data],axis=0) #存储每一页数据
exit()
abstracts.reset_index(inplace=True,drop=True) # 重置index
abstracts.to_csv('abstracts.csv',index=False)

xinzi = pd.DataFrame() # 存储数据
for i in range(42):
    b = [i.text for i in web.find_elements_by_css_selector('body > div:nth-child(4) > div.j_result > div > div.leftbox > div:nth-child(4) > div.j_joblist > div > a > p.info > span.sal')] # 薪资
    # 定义一个等待的对象
    wait = WebDriverWait(web,10) 
    # 调用等待对象，设置等待条件
    button = wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR,'body > div:nth-child(4) > div.j_result > div > div.j_tlc > div.tright > div.rt.rt_page')))
    # 点击下一页，实现翻页
    button.click()
    data = pd.DataFrame({'薪资':b})
    xinzi = pd.concat([xinzi,data],axis=0)
xinzi.reset_index(inplace=True,drop=True) # 重置index
xinzi.to_csv('xinzi.csv',index=False)
"""