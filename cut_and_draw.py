#coding:gbk;
import pandas as pd
import numpy as np
import re
import newphras3

def check2(y):
    if bigrams_lst.isin([y.group(0)]).any():
        return y.group(0).replace(" ","")
    else:
        return y.group(0)

try:
    import jieba_fast as ba
    import jieba_fast.analyse
    print("imported jieba_fast")
except:
    import jieba as ba
    jieba.analyse
    print("imported jieba")
    print("HINT:you can install jieba_fast to improve performance\n"
 "   Clone jieba_fast from github with its binary distributes\n"
 "   Or run `pip install jieba_fast' if you have Visual C++ installed higher than 2015")
#使用jieba_fast 代替 jieba节省时间
#不过jieba_fast依赖visual c++ 2015 或以上

df=pd.read_csv('abstracts_lv_clear.csv')
df=df.drop(columns=[i for i in df.columns if re.match(r'vs\d+',i) is not None])

df["cut1"]=df["abstracts"].apply(lambda x:(ba.lcut(x)))
df["cut"]=df["cut1"].apply(lambda x:" ".join(x))
k=0
while 1: #筛phrase
    k+=1;print(k)
    while 1:
        a=df["cut"].apply(len)
        df["cut"]=df["cut"].apply(lambda x: re.sub(r"\s+"," ",x))
        b=df["cut"].apply(len)
        if (a==b).all(): break
    import newphras3
    corpus=list(df.cut1)
    all_bigrams,_=newphras3.getPhrases(corpus)
    
    bigrams_lst=[]
    for i in all_bigrams:
        bigrams_lst.extend(i)
    bigrams_lst=pd.Series(bigrams_lst).drop_duplicates()
    bigrams_lst.apply(ba.add_word)
    #可以循环以筛3-word phrase
    
    if 1:         
        df["cut2phrase"]=df["cut"]
        if len(bigrams_lst)>0:
            df["cut2phrase"]=df["cut2phrase"].apply(lambda x: re.sub(r"[^ ]+ [^ ]+",check2,x))
            while 1:
                a=df["cut2phrase"].apply(len)
                df["cut2phrase"]=df["cut2phrase"].apply(lambda x: re.sub(r"[^ ]+ [^ ]+",check2,x))
                b=df["cut2phrase"].apply(len)
                if (a==b).all(): break
            a=df.cut.apply(len);b=df.cut2phrase.apply(len)
            if (a==b).all(): break
            df.cut=df.cut2phrase
            df.cut1=df.cut2phrase.apply(lambda x:x.split())

df.to_csv("abstracts_phrase.csv")
